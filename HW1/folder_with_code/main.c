//
//  main.c
//  gameoflife
//
//  Created by Pedro RP on 20/09/16.
//  Copyright © 2016 Pedro RP. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

#include "config.h"
#include "game.h"

int main(int argc, char * argv[]) {
    //printf("Parsing options..\n");
    
    GameConfig* config = game_config_new_from_cli(argc, argv);
    
    Game* game = game_new();
    
    game_parse_board(game, config);
    
    int i = 0, board_stag=0;
    for( ; i < config->generations; i++){
        if(config->debug){
            printf("%d\n",i);//print generation
            game_print_board(game);
        }
        if((board_stag=game_tick(game)))break;
    }
    
    if(!config->silent && (!board_stag || !config->debug)){
        printf("%d\n",i);//print generation
        game_print_board(game);
    }
    
    game_free(game);
    game_config_free(config);
    
    return 0;
}
