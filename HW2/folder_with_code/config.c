//
//  config.c
//  gameoflife
//
//  Created by Pedro RP on 20/09/16.
//  Copyright © 2016 Pedro RP. All rights reserved.
//

#include "config.h"
#include <unistd.h>

#define DEFAULT_GENERATIONS 20
#define DEFAULT_DEBUG       0
#define DEFAULT_SILENT      0


/**
 * Frees memory allocated for a GameConfig structure.
 *
 * @param config Pointer to a GameConfig structure.
 */
void game_config_free(GameConfig *config){
    free(config);
}

/**
 * Returns the number of generations for which to run the game.
 *
 * @param config Pointer to a GameConfig structure.
 *
 * @return The number of generations for which to run.
 */
size_t game_config_get_generations(GameConfig *config){
    return config->generations;
}

/**
 * Parses the command line and create a new GameConfig from it.
 *
 * @param argc Number of command line arguments.
 * @param argv Array of command line arguments.
 *
 * @return A new GameConfig pointer.
 */
GameConfig *game_config_new_from_cli(int argc, char *argv[]){
    GameConfig* gc = (GameConfig*)malloc(sizeof(GameConfig));
    
    //SET DEFAULTS
    gc->generations = DEFAULT_GENERATIONS;
    gc->debug       = DEFAULT_DEBUG;
    gc->silent      = DEFAULT_SILENT;
    gc->input_file  = NULL;
    
    for(char c; (c = getopt(argc, argv, "dsn:")) != -1;){
        switch (c) {
            case 'd':
                gc->debug = 1;
                printf("\tUsing debug mode.\n");
                break;
            case 's':
                gc->silent = 1;
                printf("\tUsing silent mode.\n");
                break;
            case 'n':
                gc->generations = atoi(optarg);
                printf("\tNumber of generations set to %zu\n", gc->generations);
                break;
            default:
                break;
        }
    }
    
    char* filename = argv[optind];
    printf("\tOpening file %s\n", filename);
    FILE* f = fopen(filename, "r");
    if(f==NULL)exit(5);
    printf("\tOpened file %s\n", argv[optind]);
    gc->input_file=f;
    
    putchar('\n');
    return gc;
}





