//
//  game.c
//  gameoflife
//
//  Created by Pedro RP on 20/09/16.
//  Copyright © 2016 Pedro RP. All rights reserved.
//

#include "game.h"
#include <stdio.h>
#include <string.h>
#include <cilk/cilk.h>

#define _ACELL(game, r, c) game->board[((r)*(game->cols)) + (c)]

#define COLS_TAG "Cols"
#define ROWS_TAG "Rows"

//Helper functions
/**
 * Count neighbors of cell
 * @return number of neighbors
 */
int count_neighbors(Game *game, int row, int col){
    int neigbors = 0;
    for(int i=-1; i<=1; i++)
        for(int j=-1; j<=1; j++){
            if(i==0 && j==0)continue;
            //Wrap around
            int r = row + i, c = col + j;
            
            if( r < 0 )
                r = game->rows-1;
            else if( r >= game->rows )
                r = 0;
            if( c < 0 )
                c = game->cols-1;
            else if( c >= game->cols )
                c = 0;
            
            if(_ACELL(game, r, c)) neigbors++;
        }
    return neigbors;
}

/**
 * Read line for "key : value" pair
 * @return number of neighbors
 */
int read_pair(FILE* file, char* key, char* value){
    char* DEL = ":";
    char line[512];
    char *k, *v;
    if(fgets(line, sizeof(line), file)){
        k = strtok(line, DEL);
        v = strtok(NULL, DEL);
        if(k==NULL || v==NULL) return 0;
        strcpy(key, k);
        strcpy(value, v);
    }else{return -1;}
    return 1;
}

/**
 * Parses a board file into an internal representation.
 *
 * Currently, this function only parses the custom file format
 * used by the program, but it should be trivial to add other
 * file formats.
 *
 * @param game Pointer to a Game structure.
 * @param config Pointer to a GameConfig structure.
 *
 * @retval 0 The board file was parsed correctly.
 * @retval 1 The board file could not be parsed.
 */
int game_parse_board(Game *game, GameConfig *config){
    if (config->input_file == NULL) return 1;
    
    char key[15], val[15];
    int rows = 0, cols = 0;
    //Scan for number of rows and columns
    while( cols == 0 || rows == 0 ){
        if( read_pair(config->input_file, key, val) < 0 ) {break;}
        if(strncmp(ROWS_TAG, key, strlen(ROWS_TAG)) == 0){
            //printf(":Rows= %s", val);
            rows = atoi(val);
        }else if(strncmp(COLS_TAG, key, strlen(COLS_TAG)) == 0){
            //printf(":Cols= %s", val);
            cols = atoi(val);
        }
    }
    //printf("Cols= %d\n", cols);printf("Rows= %d\n", rows);
    if(rows == 0 || cols == 0)return 1;
    
    //Create board (all cells dead)
    char* new_board = (char*)calloc(rows * cols, sizeof(char));
    game->board = new_board;
    
    game->cols = cols; game->rows = rows;
    //Fill board
    //int i=0;
    /*for(char c=fgetc(config->input_file); c!=EOF; c=fgetc(config->input_file) ){
     if(c=='#')game->board[i++]=1;
     else if(c=='.')game->board[i++]=0;
     putchar(c);
     }*/
    char l[10000];
    for(int r=0;r<rows;r++){
        fgets(l, sizeof(l), config->input_file);
        for(int c=0;c<cols && l[c];c++){
            if(l[c]=='#')_ACELL(game, r, c) = 1;
        }
    }
    return 0;
}

/**
 * Prints the current state of the board.
 *
 * @param game Pointer to a Game structure.
 */
void game_print_board(Game *game){
    for(int r=0; r<game->rows; r++){
        for(int c=0; c<game->cols; c++)
            if( _ACELL(game, r, c) )
                putchar('#');
            else
                putchar('.');
        putchar('\n');
    }
    putchar('\n');
}

/**
 * Allocates memory for a new Game structure.
 *
 * @return A new Game pointer.
 */
Game *game_new(void){
    return (Game*)malloc(sizeof(Game));
}

/**
 * Frees memory allocated to a Game structure.
 *
 * @param game Pointer to be freed.
 */
void game_free(Game *game){
    free(game->board);
}

/**
 *Gets current state of cell
 *
 * @retval 1 if alive
 * @retval 0 if dead
 */
int game_cell_state(Game *game, int row, int col){
    char s = _ACELL(game, row, col);
    return s;
}

/**
 * Checks whether a given board position is in an alive state.
 *
 * @param game Pointer to a Game structure.
 * @param row  The row number.
 * @param col  The column number.
 *
 * @retval 0 The position is in a dead state.
 * @retval 1 The position is in an alive state.
 */
int game_cell_is_alive(Game *game, size_t row, size_t col){
    int neigbors = count_neighbors(game, (int)row, (int)col);
    //printf("%d",neigbors);
    if(neigbors == 3)
        return 1;
    else return 0;
}

/**
 * Checks whether a given board position is in a dead state.
 *
 * @param game Pointer to a Game structure.
 * @param row  The row number.
 * @param col  The column number.
 *
 * @retval 0 The position is in an alive state.
 * @retval 1 The position is in a dead state.
 */
int game_cell_is_dead(Game *game, size_t row, size_t col){
    int neigbors = count_neighbors(game, (int)row, (int)col);
    //printf("%d",neigbors);
    if(neigbors > 3 || neigbors < 2)
        return 1;
    else return 0;
}

/**
 * Sets a specific position in the board to an alive state.
 *
 * @param game Pointer to a Game structure.
 * @param row  The row number.
 * @param col  The column number.
 */
void game_cell_set_alive(Game *game, size_t row, size_t col){
    _ACELL(game, row, col) = 0x1;
}

/**
 * Sets a specific position in the board to a dead state.
 *
 * @param game Pointer to a Game structure.
 * @param row  The row number.
 * @param col  The column number.
 */
void game_cell_set_dead(Game *game, size_t row, size_t col){
    _ACELL(game, row, col) = 0x0;
}

/**
 * Advances the cell board to a new generation (causes a 'tick').
 *
 * @param game Pointer to a Game structure.
 *
 * @retval 0 The tick has happened successfully.
 * @retval 1 The tick could not happen correctly.
 */
int game_tick(Game *game){
    //create new board
    char* new_board = (char*)malloc(game->cols*game->rows);
    //create old game struct
    Game _pg; Game* previous_generation = &_pg; memcpy(previous_generation, game, sizeof(Game));
    //substitute board for new
    game->board = new_board;
    
    //printf("start tick\n");
    //game_print_board(previous_generation);
    
    int generation_changed = 0;
    
    cilk_for(int r=0; r<game->rows; r++){
        for(int c=0; c<game->cols; c++){
            if( game_cell_state(previous_generation, r, c) ){
                //Cell is alive in previous generation
                if(game_cell_is_dead(previous_generation, r, c)){
                    //kill cell in new generation
                    game_cell_set_dead(game, r, c);
                    generation_changed = 1;
                }else{
                    //keep alive in new gen
                    game_cell_set_alive(game, r, c);
                }
                
            }else{
                //Cell is dead in previous gen
                if(game_cell_is_alive(previous_generation, r, c)){
                    //cell in set alive
                    game_cell_set_alive(game, r, c);
                    generation_changed = 1;
                }else{
                    //cell stays dead in new generation
                    game_cell_set_dead(game, r, c);
                }
            }
            /**/
        }
        //putchar('\n');
    }
    game_free(previous_generation);
    
    return !generation_changed;
}















